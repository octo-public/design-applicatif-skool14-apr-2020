import * as assert from 'assert'
import { expect } from 'chai'
import { EventPublisher } from '../../src/magic-bus/event/EventPublisher'
import { NotRegisterEventError } from '../../src/magic-bus/event/notRegisterEvent.error'
import { InternalEventPublisher } from '../../src/magic-bus/internal.eventPublisher'
import { MyTestEvent } from './event/myTest.event'
import { MyTestEventPublisherMiddleware } from './event/myTest.eventPublisherMiddleware'
import { MyTest2EventListener } from './event/myTest2EventListener'
import { MyTest3Event } from './event/myTest3.event'
import { MyTest3EventPublisherMiddleware } from './event/myTest3.eventPublisherMiddleware'
import { MyTest3EventListener } from './event/myTest3EventListener'
import { MyTestEventListener } from './event/myTestEventListener'
import { MyTestUnknownEvent } from './event/myTestUnknown.event'

describe('Event publisher', () => {
    describe('publish some events', () => {
        it('which exist', () => {
            const myTestEventListener: MyTestEventListener   = new MyTestEventListener()
            const myTest2EventListener: MyTest2EventListener = new MyTest2EventListener()
            const myTest3EventListener: MyTest3EventListener = new MyTest3EventListener()

            const internalEventPublisher: EventPublisher = new InternalEventPublisher()
                .registerListeners({
                    [new MyTestEvent().name] : [myTestEventListener, myTest2EventListener],
                    [new MyTest3Event().name]: [myTest3EventListener]
                })

            internalEventPublisher.publish(new MyTestEvent())
            expect(myTestEventListener.isCalledWithMyTestEvent).to.be.true
            expect(myTest2EventListener.isCalledWithMyTestEvent).to.be.true

            internalEventPublisher.publish(new MyTest3Event())
            expect(myTest3EventListener.isCalledWithMyTest3Event).to.be.true
        })

        it('which does not register', async () => {
            try {
                await new InternalEventPublisher().publish(new MyTestUnknownEvent())
                assert.fail()
            } catch (e) {
                expect(e).to.be.instanceof(NotRegisterEventError)
                expect(e.message).to.eql('The published event is not registered')
            }
        })
    })

    it('with some middleware', () => {
        const myTestEventListener: MyTestEventListener   = new MyTestEventListener()
        const myTest3EventListener: MyTest3EventListener = new MyTest3EventListener()

        const myTestEventPublisherMiddleware: MyTestEventPublisherMiddleware   = new MyTestEventPublisherMiddleware()
        const myTest3EventPublisherMiddleware: MyTest3EventPublisherMiddleware = new MyTest3EventPublisherMiddleware()

        const internalEventPublisher: EventPublisher = new InternalEventPublisher()
            .registerListeners({
                [new MyTestEvent().name] : [myTestEventListener],
                [new MyTest3Event().name]: [myTest3EventListener]
            })
            .registerMiddleware([
                myTestEventPublisherMiddleware,
                myTest3EventPublisherMiddleware
            ])

        internalEventPublisher.publish(new MyTestEvent())
        internalEventPublisher.publish(new MyTest3Event())

        expect(myTestEventPublisherMiddleware.isCalledForEvent).to.be.equal(new MyTestEvent().name)
        expect(myTest3EventPublisherMiddleware.isCalledForEvent).to.be.equal(new MyTest3Event().name)

        expect(myTestEventListener.isCalledWithMyTestEvent).to.be.true
        expect(myTest3EventListener.isCalledWithMyTest3Event).to.be.true
    })
})
