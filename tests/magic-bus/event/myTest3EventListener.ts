import { EventListener } from '../../../src/magic-bus/event/EventListener'
import { MyTest3Event }  from './myTest3.event'

export class MyTest3EventListener implements EventListener {
    isCalledWithMyTest3Event: boolean = false

    listen(event: MyTest3Event): void {
        this.isCalledWithMyTest3Event = true
    }
}
