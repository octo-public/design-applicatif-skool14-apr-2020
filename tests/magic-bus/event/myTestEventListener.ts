import { EventListener } from '../../../src/magic-bus/event/EventListener'
import { MyTestEvent }   from './myTest.event'

export class MyTestEventListener implements EventListener {
    isCalledWithMyTestEvent: boolean = false

    listen(event: MyTestEvent): void {
        this.isCalledWithMyTestEvent = true
    }
}
