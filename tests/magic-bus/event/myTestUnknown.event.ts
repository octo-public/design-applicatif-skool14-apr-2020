import { Event } from '../../../src/magic-bus/event/Event'

export class MyTestUnknownEvent implements Event {
    readonly name: string    = 'MyTestUnknownEvent'
    readonly createdAt: Date = new Date()
}
