import { Event }                    from '../../../src/magic-bus/event/Event'
import { EventPublisherMiddleware } from '../../../src/magic-bus/event/EventPublisherMiddleware'
import { MyTestEvent }              from './myTest.event'

export class MyTestEventPublisherMiddleware implements EventPublisherMiddleware {
    isCalledForEvent: string = ''

    handle(event: Event): void {
        if (event.name === new MyTestEvent().name)
            this.isCalledForEvent = event.name
    }
}
