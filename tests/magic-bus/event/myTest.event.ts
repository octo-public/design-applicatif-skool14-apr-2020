import { Event } from '../../../src/magic-bus/event/Event'

export class MyTestEvent implements Event {
    readonly name: string = 'MyTestEvent'

    constructor(private _at: Date = new Date()) {
    }

    get createdAt(): Date {
        return this._at
    }
}
