import { Event } from '../../../src/magic-bus/event/Event'

export class MyTest3Event implements Event {
    readonly name: string = 'MyTest3Event'

    constructor(private _at: Date = new Date()) {
    }

    get createdAt(): Date {
        return this._at
    }
}
