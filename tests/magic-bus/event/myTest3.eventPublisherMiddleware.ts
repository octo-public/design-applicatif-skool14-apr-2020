import { Event }                    from '../../../src/magic-bus/event/Event'
import { EventPublisherMiddleware } from '../../../src/magic-bus/event/EventPublisherMiddleware'
import { MyTest3Event }             from './myTest3.event'

export class MyTest3EventPublisherMiddleware implements EventPublisherMiddleware {
    isCalledForEvent: string = ''

    handle(event: Event): void {
        if (event.name === new MyTest3Event().name)
            this.isCalledForEvent = event.name
    }
}
