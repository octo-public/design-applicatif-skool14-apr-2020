import * as assert                      from 'assert'
import { expect }                       from 'chai'
import { InternalQueryReader }          from '../../src/magic-bus/internal.queryReader'
import { NotRegisterQueryError }        from '../../src/magic-bus/query/notRegisterQuery.error'
import { QueryReader }                  from '../../src/magic-bus/query/QueryReader'
import { MyTestQuery }                  from './query/myTest.query'
import { MyTestQueryHandler }           from './query/myTest.queryHandler'
import { MyTestQueryReaderMiddleware }  from './query/myTest.queryReaderMiddleware'
import { MyTest2Query }                 from './query/myTest2.query'
import { MyTest2QueryHandler }          from './query/myTest2.queryHandler'
import { MyTest2QueryReaderMiddleware } from './query/myTest2.queryReaderMiddleware'
import { MyTestUnknownQuery }           from './query/myTestUnknwon.Query'

describe('Query reader', () => {
    describe('dispatch some queries', () => {
        it('which exist', async () => {
            const myTestQueryHandler: MyTestQueryHandler   = new MyTestQueryHandler()
            const myTest2QueryHandler: MyTest2QueryHandler = new MyTest2QueryHandler()

            const internalQueryReader: QueryReader = new InternalQueryReader()
                .registerHandlers({
                    [new MyTestQuery().name] : myTestQueryHandler,
                    [new MyTest2Query().name]: myTest2QueryHandler
                })

            const result = await internalQueryReader.dispatch<string>(new MyTestQuery())
            expect(result).to.eql('hello')

            const result2 = await internalQueryReader.dispatch<boolean>(new MyTest2Query())
            expect(result2).to.be.false
        })

        it('which does not register', async () => {
            try {
                await new InternalQueryReader().dispatch(new MyTestUnknownQuery())
                assert.fail()
            } catch (e) {
                expect(e).to.be.instanceof(NotRegisterQueryError)
                expect(e.message).to.eql('The dispatched query is not registered')
            }
        })
    })

    it('with some middleware', async () => {
        const myTestQueryHandler: MyTestQueryHandler   = new MyTestQueryHandler()
        const myTest2QueryHandler: MyTest2QueryHandler = new MyTest2QueryHandler()

        const myTestQueryReaderMiddleware: MyTestQueryReaderMiddleware   = new MyTestQueryReaderMiddleware()
        const myTest2QueryReaderMiddleware: MyTest2QueryReaderMiddleware = new MyTest2QueryReaderMiddleware()

        const internalQueryReader: QueryReader = new InternalQueryReader()
            .registerHandlers({
                [new MyTestQuery().name] : myTestQueryHandler,
                [new MyTest2Query().name]: myTest2QueryHandler
            })
            .registerMiddleware([
                myTestQueryReaderMiddleware,
                myTest2QueryReaderMiddleware
            ])

        const result  = await internalQueryReader.dispatch<string>(new MyTestQuery())
        const result2 = await internalQueryReader.dispatch<boolean>(new MyTest2Query())

        expect(myTestQueryReaderMiddleware.isCalledForQuery).to.eql(new MyTestQuery().name)
        expect(myTest2QueryReaderMiddleware.isCalledForQuery).to.eql(new MyTest2Query().name)

        expect(result).to.eql('hello')
        expect(result2).to.be.false
    })
})
