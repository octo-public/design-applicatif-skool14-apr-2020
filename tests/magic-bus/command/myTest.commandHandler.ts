import { CommandHandler } from '../../../src/magic-bus/command/CommandHandler'
import { MyTestCommand }  from './myTest.command'

export class MyTestCommandHandler implements CommandHandler {
    isCalledWithMyTestCommand: boolean = false

    async execute(command: MyTestCommand): Promise<void> {
        this.isCalledWithMyTestCommand = true
    }
}
