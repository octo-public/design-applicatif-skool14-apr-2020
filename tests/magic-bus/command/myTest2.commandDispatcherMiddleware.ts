import { Command }                     from '../../../src/magic-bus/command/Command'
import { CommandDispatcherMiddleware } from '../../../src/magic-bus/command/CommandDispatcherMiddleware'
import { MyTestCommand }               from './myTest.command'

export class MyTest2CommandDispatcherMiddleware implements CommandDispatcherMiddleware {
    isCalledForCommand: string = ''

    handle(command: Command): void {
        if (command.name !== new MyTestCommand().name)
            this.isCalledForCommand = command.name
    }
}
