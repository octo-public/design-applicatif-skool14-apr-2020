import { CommandHandler } from '../../../src/magic-bus/command/CommandHandler'
import { MyTest2Command } from './myTest2.command'

export interface MyTest2CommandHandlerResponse { key: string }

export class MyTest2CommandHandler implements CommandHandler<MyTest2CommandHandlerResponse> {
    isCalledWithMyTest2Command: boolean = false

    async execute(command: MyTest2Command): Promise<MyTest2CommandHandlerResponse> {
        this.isCalledWithMyTest2Command = true
        return { key: 'my test 2 command result' }
    }
}
