import { Command }                     from '../../../src/magic-bus/command/Command'
import { CommandDispatcherMiddleware } from '../../../src/magic-bus/command/CommandDispatcherMiddleware'
import { MyTest2Command }              from './myTest2.command'

export class MyTestCommandDispatcherMiddleware implements CommandDispatcherMiddleware {
    isCalledForCommand: string = ''

    handle(command: Command): void {
        if (command.name !== new MyTest2Command().name)
            this.isCalledForCommand = command.name
    }
}
