import { Command } from '../../../src/magic-bus/command/Command'

export class MyTestCommand implements Command {
    readonly name: string = 'MyTestCommand'
}
