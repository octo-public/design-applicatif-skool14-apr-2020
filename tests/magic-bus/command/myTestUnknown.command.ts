import { Command } from '../../../src/magic-bus/command/Command'

export class MyTestUnknownCommand implements Command {
    readonly name: string = 'MyTestUnknownCommand'
}
