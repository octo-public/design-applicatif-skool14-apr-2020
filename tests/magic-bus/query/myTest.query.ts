import { Query } from '../../../src/magic-bus/query/Query'

export class MyTestQuery implements Query {
    readonly name: string = 'MyTestQuery'
}
