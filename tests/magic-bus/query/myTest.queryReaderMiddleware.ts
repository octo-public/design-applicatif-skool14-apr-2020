import { Query }                 from '../../../src/magic-bus/query/Query'
import { QueryReaderMiddleware } from '../../../src/magic-bus/query/QueryReaderMiddleware'
import { MyTestQuery }           from './myTest.query'

export class MyTestQueryReaderMiddleware implements QueryReaderMiddleware {
    isCalledForQuery: string = ''

    handle(query: Query): void {
        if (query.name === new MyTestQuery().name)
            this.isCalledForQuery = query.name
    }
}

