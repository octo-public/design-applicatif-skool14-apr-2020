import { Query } from '../../../src/magic-bus/query/Query'

export class MyTestUnknownQuery implements Query {
    readonly name: string = 'MyTestUnknownQuery'
}
