import { Query }                 from '../../../src/magic-bus/query/Query'
import { QueryReaderMiddleware } from '../../../src/magic-bus/query/QueryReaderMiddleware'
import { MyTest2Query }          from './myTest2.query'

export class MyTest2QueryReaderMiddleware implements QueryReaderMiddleware {
    isCalledForQuery: string = ''

    handle(query: Query): void {
        if (query.name === new MyTest2Query().name)
            this.isCalledForQuery = query.name
    }
}
