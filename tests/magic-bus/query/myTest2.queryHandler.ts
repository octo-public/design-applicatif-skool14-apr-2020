import { QueryHandler } from '../../../src/magic-bus/query/QueryHandler'
import { MyTest2Query } from './myTest2.query'

export class MyTest2QueryHandler implements QueryHandler<boolean> {
    async execute(query: MyTest2Query): Promise<boolean> {
        return false
    }
}
