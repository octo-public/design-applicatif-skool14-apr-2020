import { QueryHandler } from '../../../src/magic-bus/query/QueryHandler'
import { MyTestQuery }  from './myTest.query'

export class MyTestQueryHandler implements QueryHandler<string> {
    async execute(query: MyTestQuery): Promise<string> {
        return 'hello'
    }
}
