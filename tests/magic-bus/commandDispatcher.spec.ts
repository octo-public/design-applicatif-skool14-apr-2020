import * as assert                            from 'assert'
import { expect }                             from 'chai'
import { CommandDispatcher }                  from '../../src/magic-bus/command/CommandDispatcher'
import { NotRegisterCommandError }            from '../../src/magic-bus/command/notRegisterCommand.error'
import { InternalCommandDispatcher }          from '../../src/magic-bus/internal.commandDisptacher.spec'
import { MyTestCommand }                      from './command/myTest.command'
import { MyTestCommandDispatcherMiddleware }  from './command/myTest.commandDispatcherMiddleware'
import { MyTestCommandHandler }               from './command/myTest.commandHandler'
import { MyTest2Command }                     from './command/myTest2.command'
import { MyTest2CommandDispatcherMiddleware }                   from './command/myTest2.commandDispatcherMiddleware'
import { MyTest2CommandHandler, MyTest2CommandHandlerResponse } from './command/myTest2.commandHandler'
import { MyTestUnknownCommand }                                 from './command/myTestUnknown.command'

describe('Command dispatcher', () => {
    describe('dispatch some commands', () => {
        it('which exist', async () => {
            const myTestCommandHandler: MyTestCommandHandler   = new MyTestCommandHandler()
            const myTest2CommandHandler: MyTest2CommandHandler = new MyTest2CommandHandler()

            const internalCommandDispatcher: CommandDispatcher = new InternalCommandDispatcher()
                .registerHandlers({
                    [new MyTestCommand().name] : myTestCommandHandler,
                    [new MyTest2Command().name]: myTest2CommandHandler
                })

            await internalCommandDispatcher.dispatch(new MyTestCommand())
            expect(myTestCommandHandler.isCalledWithMyTestCommand).to.be.true

            const result = await internalCommandDispatcher.dispatch<MyTest2CommandHandlerResponse>(new MyTest2Command())
            expect(myTest2CommandHandler.isCalledWithMyTest2Command).to.be.true
            expect(result).to.eql({ key: 'my test 2 command result' })
        })

        it('which does not register', async () => {
            try {
                await new InternalCommandDispatcher().dispatch(new MyTestUnknownCommand())
                assert.fail()
            } catch (e) {
                expect(e).to.be.instanceof(NotRegisterCommandError)
                expect(e.message).to.eql('The dispatched command is not registered')
            }
        })
    })

    it('with some middleware', async () => {
        const myTestCommandHandler: MyTestCommandHandler   = new MyTestCommandHandler()
        const myTest2CommandHandler: MyTest2CommandHandler = new MyTest2CommandHandler()

        const myTestCommandDispatcherMiddleware: MyTestCommandDispatcherMiddleware   = new MyTestCommandDispatcherMiddleware()
        const myTest2CommandDispatcherMiddleware: MyTest2CommandDispatcherMiddleware = new MyTest2CommandDispatcherMiddleware()

        const internalCommandDispatcher: CommandDispatcher = new InternalCommandDispatcher()
            .registerHandlers({
                [new MyTestCommand().name] : myTestCommandHandler,
                [new MyTest2Command().name]: myTest2CommandHandler
            })
            .registerMiddleware([
                myTestCommandDispatcherMiddleware,
                myTest2CommandDispatcherMiddleware
            ])

        await internalCommandDispatcher.dispatch(new MyTestCommand())
        await internalCommandDispatcher.dispatch(new MyTest2Command())

        expect(myTestCommandDispatcherMiddleware.isCalledForCommand).to.eql(new MyTestCommand().name)
        expect(myTest2CommandDispatcherMiddleware.isCalledForCommand).to.eql(new MyTest2Command().name)

        expect(myTestCommandHandler.isCalledWithMyTestCommand).to.be.true
        expect(myTest2CommandHandler.isCalledWithMyTest2Command).to.be.true
    })
})
