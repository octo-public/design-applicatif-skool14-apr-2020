import { Command } from '../magic-bus/command/Command'
import { CommandDispatcherMiddleware } from '../magic-bus/command/CommandDispatcherMiddleware'
import { CommandHandler } from '../magic-bus/command/CommandHandler'
import { Event } from '../magic-bus/event/Event'
import { EventListener } from '../magic-bus/event/EventListener'
import { EventPublisher } from '../magic-bus/event/EventPublisher'
import { InternalCommandDispatcher } from '../magic-bus/internal.commandDisptacher.spec'
import { InternalEventPublisher } from '../magic-bus/internal.eventPublisher'

class MakeReservationCommand implements Command {
  readonly name = 'MakeReservationCommand'

  constructor(roomNumber: string, checkOutAt: Date) {
  }
}

class ReservationMadeEvent implements Event {
  readonly createdAt: Date = new Date()
  readonly name = 'ReservationMadeEvent'
}

interface ReservationRepository {
  save(): void
}

class InMemoryReservationRepository implements ReservationRepository {
  save(): void {
    console.log('J ai sauvegardé dans la base numéro 1')
  }
}

class ProjectionReservationRepository implements ReservationRepository {
  save(): void {
    console.log('J ai projeté mon événement')
  }
}

class MakeReservationHandler implements CommandHandler {
  constructor(private eventPublisher: EventPublisher, private reservationRepository: ReservationRepository) {
  }

  async execute(command: MakeReservationCommand): Promise<void> {
    /*
      Do many thing
      business rules...
      save to repo
     */
    console.log('Je suis passé par là :)')
    this.reservationRepository.save()
    this.eventPublisher.publish(new ReservationMadeEvent())
  }
}

class ReservationMadeHandler implements EventListener {
  constructor(private reservationRepository: ReservationRepository) {
  }
  listen(event: ReservationMadeEvent): void {
    this.reservationRepository.save()
  }
}

class ReservationMade2Handler implements EventListener {
  listen(event: ReservationMadeEvent): void {
    console.log('J ai envoyé un sms')
  }
}

class LoggerCommandMiddleware implements CommandDispatcherMiddleware {
  handle(command: Command): void {
    console.log(command)
  }
}

const eventPublisher = new InternalEventPublisher()
  .registerListeners({
    [ReservationMadeEvent.name]: [new ReservationMadeHandler(new ProjectionReservationRepository()), new ReservationMade2Handler()]
  })

const commandDispatcher = new InternalCommandDispatcher()
  .registerMiddleware([
    new LoggerCommandMiddleware()
  ])
  .registerHandlers({
    [MakeReservationCommand.name]: new MakeReservationHandler(eventPublisher, new InMemoryReservationRepository())
  })


commandDispatcher.dispatch(new MakeReservationCommand('101', new Date('2020-01-31')))


