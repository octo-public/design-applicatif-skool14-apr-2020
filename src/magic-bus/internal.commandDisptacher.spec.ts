import { Command }                     from './command/Command'
import { CommandDispatcher }           from './command/CommandDispatcher'
import { CommandDispatcherMiddleware } from './command/CommandDispatcherMiddleware'
import { CommandHandler }              from './command/CommandHandler'
import { NotRegisterCommandError }     from './command/notRegisterCommand.error'

export class InternalCommandDispatcher implements CommandDispatcher {
    private handlers: { [command: string]: CommandHandler } = {}
    private middleware: CommandDispatcherMiddleware[]       = []

    registerHandlers(handlers: { [command: string]: CommandHandler }): CommandDispatcher {
        this.handlers = handlers
        return this
    }

    registerMiddleware(middleware: CommandDispatcherMiddleware[]): CommandDispatcher {
        this.middleware = middleware
        return this
    }

    async dispatch<R>(command: Command): Promise<R> {
        if (this.handlers[command.name] === undefined)
            throw new NotRegisterCommandError()

        this.middleware.forEach(m => m.handle(command))

        return this.handlers[command.name].execute(command)
    }
}
