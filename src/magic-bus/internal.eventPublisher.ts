import { Event } from './event/Event'
import { EventListener } from './event/EventListener'
import { EventPublisher } from './event/EventPublisher'
import { EventPublisherMiddleware } from './event/EventPublisherMiddleware'
import { NotRegisterEventError } from './event/notRegisterEvent.error'

export class InternalEventPublisher implements EventPublisher {
    private listeners: { [event: string]: EventListener[] } = {}
    private middleware: EventPublisherMiddleware[]          = []

    constructor() {
    }

    registerListeners(eventListeners: { [event: string]: EventListener[] }): EventPublisher {
        this.listeners = eventListeners
        return this
    }

    registerMiddleware(eventPublisherMiddleware: EventPublisherMiddleware[]): EventPublisher {
        this.middleware = eventPublisherMiddleware
        return this
    }

    async publish(event: Event): Promise<void> {
        if (this.listeners[event.name] === undefined)
            throw new NotRegisterEventError()

        this.middleware.forEach(m => m.handle(event))

        this.listeners[event.name].forEach(l => l.listen(event))
    }
}
