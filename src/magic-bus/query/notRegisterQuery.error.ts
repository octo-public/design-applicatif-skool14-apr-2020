export class NotRegisterQueryError extends Error {
    constructor() {
        super('The dispatched query is not registered')
    }
}
