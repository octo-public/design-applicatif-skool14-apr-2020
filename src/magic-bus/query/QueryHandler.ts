import { Query } from './Query'

export interface QueryHandler<R = any> {
    execute(query: Query): Promise<R>
}
