import { NotRegisterQueryError } from './query/notRegisterQuery.error'
import { Query }                 from './query/Query'
import { QueryHandler }          from './query/QueryHandler'
import { QueryReader }           from './query/QueryReader'
import { QueryReaderMiddleware } from './query/QueryReaderMiddleware'

export class InternalQueryReader implements QueryReader {
    private queryHandlers: { [query: string]: QueryHandler } = {}
    private middleware: QueryReaderMiddleware[]              = []

    registerHandlers(queryHandlers: { [query: string]: QueryHandler }): QueryReader {
        this.queryHandlers = queryHandlers
        return this
    }

    registerMiddleware(queryReaderMiddleware: QueryReaderMiddleware[]): QueryReader {
        this.middleware = queryReaderMiddleware
        return this
    }

    async dispatch<R>(query: Query): Promise<R> {
        if (this.queryHandlers[query.name] === undefined)
            throw new NotRegisterQueryError()

        this.middleware.forEach(m => m.handle(query))

        return this.queryHandlers[query.name].execute(query)
    }
}
