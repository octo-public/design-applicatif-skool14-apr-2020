import { Command } from './Command'

export interface CommandHandler<R = void | any> {
    execute(command: Command): Promise<R>
}
