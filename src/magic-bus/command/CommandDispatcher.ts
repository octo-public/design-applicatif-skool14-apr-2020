import { Command }                     from './Command'
import { CommandDispatcherMiddleware } from './CommandDispatcherMiddleware'
import { CommandHandler }              from './CommandHandler'

export interface CommandDispatcher {
    registerHandlers(commandHandlers: { [key: string]: CommandHandler }): CommandDispatcher

    registerMiddleware(commandDispatcherMiddleware: CommandDispatcherMiddleware[]): CommandDispatcher

    dispatch<R = void>(command: Command): Promise<R>
}
