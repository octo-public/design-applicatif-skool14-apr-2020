import { Event } from './Event'

export interface EventListener {
    listen(event: Event): void
}
