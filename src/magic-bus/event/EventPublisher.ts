import { Event }                    from './Event'
import { EventListener }            from './EventListener'
import { EventPublisherMiddleware } from './EventPublisherMiddleware'

export interface EventPublisher {
    registerListeners(eventListeners: { [event: string]: EventListener[] }): EventPublisher

    registerMiddleware(eventPublisherMiddleware: EventPublisherMiddleware[]): EventPublisher

    publish(event: Event): Promise<void>
}
